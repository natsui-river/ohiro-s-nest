(function (window, document, listeners_prop_name) {
  if ((!window.addEventListener || !window.removeEventListener) && window.attachEvent && window.detachEvent) {
      /**
       * @param {*} value
       * @return {boolean}
       */
      var is_callable = function (value) {
          return typeof value === 'function';
      };
      /**
       * @param {!Window|HTMLDocument|Node} self
       * @param {EventListener|function(!Event):(boolean|undefined)} listener
       * @return {!function(Event)|undefined}
       */
      var listener_get = function (self, listener) {
          var listeners = listener[listeners_prop_name];
          if (listeners) {
              var lis;
              var i = listeners.length;
              while (i--) {
                  lis = listeners[i];
                  if (lis[0] === self) {
                      return lis[1];
                  }
              }
          }
      };
      /**
       * @param {!Window|HTMLDocument|Node} self
       * @param {EventListener|function(!Event):(boolean|undefined)} listener
       * @param {!function(Event)} callback
       * @return {!function(Event)}
       */
      var listener_set = function (self, listener, callback) {
          var listeners = listener[listeners_prop_name] || (listener[listeners_prop_name] = []);
          return listener_get(self, listener) || (listeners[listeners.length] = [self, callback], callback);
      };
      /**
       * @param {string} methodName
       */
      var docHijack = function (methodName) {
          var old = document[methodName];
          document[methodName] = function (v) {
              return addListen(old(v));
          };
      };
      /**
       * @this {!Window|HTMLDocument|Node}
       * @param {string} type
       * @param {EventListener|function(!Event):(boolean|undefined)} listener
       * @param {boolean=} useCapture
       */
      var addEvent = function (type, listener, useCapture) {
          if (is_callable(listener)) {
              var self = this;
              self.attachEvent(
                  'on' + type,
                  listener_set(self, listener, function (e) {
                      e = e || window.event;
                      e.preventDefault = e.preventDefault || function () { e.returnValue = false };
                      e.stopPropagation = e.stopPropagation || function () { e.cancelBubble = true };
                      e.target = e.target || e.srcElement || document.documentElement;
                      e.currentTarget = e.currentTarget || self;
                      e.timeStamp = e.timeStamp || (new Date()).getTime();
                      listener.call(self, e);
                  })
              );
          }
      };
      /**
       * @this {!Window|HTMLDocument|Node}
       * @param {string} type
       * @param {EventListener|function(!Event):(boolean|undefined)} listener
       * @param {boolean=} useCapture
       */
      var removeEvent = function (type, listener, useCapture) {
          if (is_callable(listener)) {
              var self = this;
              var lis = listener_get(self, listener);
              if (lis) {
                  self.detachEvent('on' + type, lis);
              }
          }
      };
      /**
       * @param {!Node|NodeList|Array} obj
       * @return {!Node|NodeList|Array}
       */
      var addListen = function (obj) {
          var i = obj.length;
          if (i) {
              while (i--) {
                  obj[i].addEventListener = addEvent;
                  obj[i].removeEventListener = removeEvent;
              }
          } else {
              obj.addEventListener = addEvent;
              obj.removeEventListener = removeEvent;
          }
          return obj;
      };

      addListen([document, window]);
      if ('Element' in window) {
          /**
           * IE8
           */
          var element = window.Element;
          element.prototype.addEventListener = addEvent;
          element.prototype.removeEventListener = removeEvent;
      } else {
          /**
           * IE < 8
           */
          //Make sure we also init at domReady
          document.attachEvent('onreadystatechange', function () { addListen(document.all) });
          docHijack('getElementsByTagName');
          docHijack('getElementById');
          docHijack('createElement');
          addListen(document.all);
      }
  }
})(window, document, 'x-ms-event-listeners');

if (!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
  }
}

(function (window, rAF, cAF) {
  var lastTime = 0, vendors = ['ms', 'moz', 'webkit', 'o'], x;

  for (x = 0; x < vendors.length && !window[rAF]; ++x) {
      window[rAF] = window[vendors[x] + 'RequestAnimationFrame'];
      window[cAF] = window[vendors[x] + 'CancelAnimationFrame']
          || window[vendors[x] + 'CancelRequestAnimationFrame'];
  }

  if (!window[rAF]) {
      window[rAF] = function (callback) {
          var currTime = new Date().getTime(),
              timeToCall = Math.max(0, 16 - (currTime - lastTime)),
              id = window.setTimeout(function () { callback(currTime + timeToCall); }, timeToCall);

          lastTime = currTime + timeToCall;

          return id;
      };
  }

  if (!window[cAF]) {
      window[cAF] = function (id) {
          window.clearTimeout(id);
      };
  }
}(this, 'requestAnimationFrame', 'cancelAnimationFrame'));

if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
	  'use strict';
	  if (this == null) {
		throw new TypeError();
	  }
	  var n, k, t = Object(this),
		  len = t.length >>> 0;
  
	  if (len === 0) {
		return -1;
	  }
	  n = 0;
	  if (arguments.length > 1) {
		n = Number(arguments[1]);
		if (n != n) { // shortcut for verifying if it's NaN
		  n = 0;
		} else if (n != 0 && n != Infinity && n != -Infinity) {
		  n = (n > 0 || -1) * Math.floor(Math.abs(n));
		}
	  }
	  if (n >= len) {
		return -1;
	  }
	  for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
		if (k in t && t[k] === searchElement) {
		  return k;
		}
	  }
	  return -1;
	};
  }



function getCookie(cookieName) {
  var name = cookieName + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var cookieArray = decodedCookie.split(';');
  for(var i = 0; i < cookieArray.length; i++) {
    var cookie = cookieArray[i].trim();
    if (cookie.indexOf(name) == 0) {
      return cookie.substring(name.length, cookie.length);
    }
  }
  return null;
}

var txtt  = getCookie("txtt");
var txtn  = getCookie("txtn");
var txtf  = getCookie("txtf");

var txttn  = getCookie("txttn");
var txtnn  = getCookie("txtnn");
var txtfn  = getCookie("txtfn");

var selectt = document.getElementById("theme");
var selectn = document.getElementById("navsoroe");
var selectf = document.getElementById("font");

var themecss = document.getElementById("themecss");
var navcss = document.getElementById("navcss");
var fontcss = document.getElementById("fontcss");

selectt.value = txttn;
selectn.value = txtnn;
selectf.value = txtfn;



  if ( txtt === "ダーク") {

  themecss.href = "settingsdark.css";

  } else if ( txtt === "ライト") {

    themecss.href = "settingslight.css";

  } else if ( txtt === "システム") {

    themecss.href = "settingssys.css";

  }


  if ( txtn === "中央揃え") {
  
       navcss.href = "settingschuo.css";
  
     } else if ( txtn === "左揃え") {
  
       navcss.href = "settingshidari.css";
  
  }


  if ( txtf === "やさしさゴシック") {

    fontcss.href = "fontyasashisa.css";
  
    } else if ( txtf === "BIZ UDPゴシック") {
  
      fontcss.href = "fontbizudpg.css";
  
    } else if ( txtf === "ヒラギノ角ゴシック") {
  
      fontcss.href = "fonthirakaku.css";
  
    } else if ( txtf === "游ゴシック") {
  
      fontcss.href = "fontyugo.css";
  
    } else if ( txtf === "メイリオ") {
  
      fontcss.href = "fontmeiryo.css";
  
    } else if ( txtf === "Noto Sans") {
  
      fontcss.href = "fontnoto.css";
  
    } else if ( txtf === "MS Pゴシック") {
  
      fontcss.href = "fontmspg.css";
  
    }




selectt.addEventListener("change", function() {

    var idt = selectt.selectedIndex;
    var txtt  = selectt.options[idt].text;
    console.log(txtt);

    document.cookie = "txtt=" + encodeURIComponent(txtt);
    document.cookie = "txttn=" + encodeURIComponent(selectt.value);

    var themecss = document.getElementById("themecss");

    if ( txtt === "ダーク") {

        themecss.href = "settingsdark.css";

      } else if ( txtt === "ライト") {

        themecss.href = "settingslight.css";

      } else if ( txtt === "システム") {

        themecss.href = "settingssys.css";

      }
});

selectn.addEventListener("change", function() {

    var idn = selectn.selectedIndex;
    var txtn  = selectn.options[idn].text;
    console.log(txtn);

    document.cookie = "txtn=" + encodeURIComponent(txtn);
    document.cookie = "txtnn=" + encodeURIComponent(selectn.value);

    var navcss = document.getElementById("navcss");

    if ( txtn === "中央揃え") {

        navcss.href = "settingschuo.css";

      } else if ( txtn === "左揃え") {

        navcss.href = "settingshidari.css";

      }
});

selectf.addEventListener("change", function() {

  var idf = selectf.selectedIndex;
  var txtf  = selectf.options[idf].text;
  console.log(txtf);

  document.cookie = "txtf=" + encodeURIComponent(txtf);
  document.cookie = "txtfn=" + encodeURIComponent(selectf.value);

  var fontcss = document.getElementById("fontcss");

  if ( txtf === "やさしさゴシック") {

    fontcss.href = "fontyasashisa.css";
  
    } else if ( txtf === "BIZ UDPゴシック") {
  
      fontcss.href = "fontbizudpg.css";
  
    } else if ( txtf === "ヒラギノ角ゴシック") {
  
      fontcss.href = "fonthirakaku.css";
  
    } else if ( txtf === "游ゴシック") {
  
      fontcss.href = "fontyugo.css";
  
    } else if ( txtf === "メイリオ") {
  
      fontcss.href = "fontmeiryo.css";
  
    } else if ( txtf === "Noto Sans") {
  
      fontcss.href = "fontnoto.css";
  
    } else if ( txtf === "MS Pゴシック") {
  
      fontcss.href = "fontmspg.css";
  
    }

  
});