function get_flash_version() {
    var version='0.0.0';
    if(navigator.plugins && navigator.mimeTypes['application/x-shockwave-flash']){
     var plugin=navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin;
     // the code below is used in SWFObject
     //var plugin=navigator.plugins['Shockwave Flash'];
     if (plugin && plugin.description) {
      // convert the description like 'Shockwave Flash 9.0 r28' into version string like '9.0.8';
      // regex is provided by SWFObject
      version=plugin.description.replace(/^[A-Za-z\s]+/, '').replace(/(\s+r|\s+b[0-9]+)/, ".");
     }
     
    } else { // in the case of Win IE
     var x='';
     try {
      // for ver.7 and later
      var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
      x=axo.GetVariable("$version");
     } catch(e) {
      try {
       // for ver.6
       axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
       x="WIN 6,0,21,0";
       /*
        * GetVariable() crashes player version 6.0.22-29, and
        * players which have those versions throws when access 
        * to AllowScriptAccess
        */
       axo.AllowScriptAccess="always";
       x=axo.GetVariable("$version");
      } catch(e) {
       if (!x.match(/^WIN/)) {
        try {
         // for 4.x,5.x
         axo=null;
         axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.3");
         // version 3 player throws when you call GetVariale().
         x=axo.GetVariable("$version");
        } catch(e) {
         if (axo) {
          // for 3.x
          x="WIN 3,0,18,0";
         } else {
          try {
           // for 2.x
           axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
           x="WIN 2,0,0,11";
          } catch(e) {
           x="WIN 0,0,0,0";
          }
         }
        }
       }
      }
     }
     // convert ActiveX version string to our version string like '9.0.28'
     version=x.replace(/^WIN /,'').replace(/,[0-9]+$/,'').replace(/,/g,'.');
    }
    // check version string format
    // Quicktime enabled Safari returns a description in natural language
    if (version.match(/^[0-9]+\.[0-9]+\.[0-9]+$/)) {
     return version;
    } else {
     return '0.0.0';
    }
   }
   document.write(get_flash_version());